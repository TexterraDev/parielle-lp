(function($) {
    jQuery.fn.lightTabs = function(options) {

        var createTabs = function() {
            var tabs = $(this);
            var tabsList = tabs.children("ul");
            var tabsContent = $('.lp__tabs').children("div");

            tabsContent.children('div').not(':first-of-type').hide();
            tabsList.children('li:first-of-type').addClass('active');

            tabsContent.each(function(){
                i = 0;

                $(this).children('div').each(function(index, element) {
              $(element).attr("data-page", index);
              i++;
            });

            });

            tabsList.each(function() {
                i = 0;

            $(this).children('li').each(function(index, element) {
              $(element).attr("data-page", index);
              i++;
              $(this).click(function() {
                    $(this).addClass('active');
                    $(this).siblings().removeClass('active');
                    var dataPage = $(this).attr('data-page');
                    $(this).parents(".lp__tabs").children('div').children('div[data-page="'+dataPage+'"]').attr('data-page', dataPage).removeAttr('style');
                    $(this).parents(".lp__tabs").children('div').children('div').not('[data-page="'+dataPage+'"]').hide();
                });
            });
            });
        };
        return this.each(createTabs);
    };
})(jQuery);