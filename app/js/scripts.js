$( document ).ready(function() {

  $(function() {
      $('.lazy').Lazy({
        effect: 'fadeIn'
      });
  });

  $('.hamburger').click(function() {
    $(this).toggleClass('is-active');
    $(this).parents('.mob__menu__wrap').siblings('.menu').slideToggle();
  });

  $('.top__slider').slick({
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 2600,
    slidesToScroll: 1,
    arrows: false,
    lazyLoad: 'progressive',
    pauseOnFocus: false,
    pauseOnHover: false,
    infinite: true,
    dots: true,
    speed: 200,
    fade: true
  });

  $('.lp__tabs').lightTabs();

  /*Scroll to block with prop on small resolution*/
  var winWidth = $(window).width();
  if (winWidth > 319 && winWidth < 1000) {
    $('.materail__block__tags__slider__item').click(function() {
      $('html, body').animate({ scrollTop: $(".material__block__tags__content__slider__item").offset().top +50 }, 800);
    });
    $('.uquip__advantages__tabs__list li').click(function() {
      $('html, body').animate({ scrollTop: $(".uquip__advantages__tabs__content").offset().top -50}, 800);
    });
  }

  $('.top__tabs__list__item').click(function() {
    $('html, body').animate({ scrollTop: $('.top__tabs__wrap').offset().top -50 }, 500);
  });
  

  /*Sync material sliders*/
  $('.material__block__tags__content__slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    adaptiveHeight: true,
    lazyLoad: 'progressive',
    fade: true,
    asNavFor: '.materail__block__tags__slider'
  });

  $('.materail__block__tags__slider').slick({
    vertical: true,
    slidesToScroll: 1,
    asNavFor: '.material__block__tags__content__slider',
    dots: false,
    mobileFirst: true,
    centerMode: true,
    focusOnSelect: true,
    responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 10
      }
    },
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }
  ]
  });

  /*Triggering the sliders by tags*/

  $('.tags a').click(function(i) {
    i.preventDefault(i);
    var dataTag = $(this).data('tag');
    $('.materail__block__tags__slider__item__tag[data-triggertag="'+dataTag+'"]').trigger('click');

    if ($(window).width() > 767) {
      $('html, body').animate({
        scrollTop: $(".material__block").offset().top +100 
      }, 800);
    } else {
      $('html, body').animate({
        scrollTop: $(".material__block__tags__content__slider").offset().top - 50
      }, 800);
    }
  });

  /*Selectenu*/
  $(".calculator__form select").selectmenu();

  /*Only numbers in input*/

  $('.input__wrap input[type="number"]').keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           // Allow: Ctrl/cmd+A
          (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+C
          (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: Ctrl/cmd+X
          (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });

  /*Increment on input*/
  $('.increment__down').click(function () {
      var $input = $(this).parents('.input__wrap').find('input.full');
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();
      return false;
  });
  $('.increment__up').click(function () {
      var $input = $(this).parents('.input__wrap').find('input.full');
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
  });

  /*Hide insta-blocks*/
  var instaBlock = $('.insta__block__element');

  if (instaBlock.length > 8) {
    $('.insta__block__element:nth-of-type(n+9)').hide();
  }

  /*Show insta-blocks*/

  $('.insta__block__more__btn').click(function(k) {
    k.preventDefault(k);
    $('.insta__block__element:nth-of-type(n+9)').fadeIn();
    $(this).fadeOut();
  });

  /*Slider by click on equip tabs item*/
  $('.equip__tabs__slider__item').click(function(){
    $('.underlay').fadeIn();
    $(this).parents('.equip__tabs__slider__wrap').addClass('slicked');

    $(this).parents('.equip__tabs__slider').slick({
      dots: false,
      arrows: true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1
    });
    actIndex = $(this).parent('.equip__tabs__slider__item__wrap').attr('data-slick-index');
    var slider = $(this).parents('.equip__tabs__slider');
    slider[0].slick.slickGoTo(parseInt(actIndex));
  });

  /*Fade underlay, unslick equip tabs item*/
  $('.equip__tabs__slider__close').click(function() {
    $('.underlay').fadeOut();
     $(this).parents('.equip__tabs__slider__wrap').removeClass('slicked');
     $(this).next('.equip__tabs__slider').slick('unslick');
  });

  /*Underlay click close everything*/
  $('.underlay').click(function() {
    if ($('.equip__tabs__slider__wrap').hasClass('slicked')) {
      $('.equip__tabs__slider__wrap').removeClass('slicked');
      $('.equip__tabs__slider').slick('unslick');
    } 
    $(this).fadeOut();
    $('.callback__popup').fadeOut();
  });

  /*Clients slider*/
  $('.clients__block__slider').slick({
    dots: false,
    infinite: true,
    mobileFirst: true,
    lazyLoad: 'progressive',
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 5
        }
      },
      {
        breakpoint: 999,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 319,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  /*Showing popup*/
  $('.header__section__callback__btn').click(function(callBack) {
    callBack.preventDefault(callBack);
    $('.underlay').fadeIn();
    $('.--callback').fadeIn();
  });

  $('.calculator__form__btn').click(function(event) {
    event.preventDefault();
    $('.underlay').fadeIn();
    $('.--calculator-order').fadeIn();
  });

  $('.callback__popup__close').click(function() {
    $(this).parent('.callback__popup').fadeOut();
    $('.underlay').fadeOut();
  });

  $('.slider__banner__callback').click(function(firstOrder) {
    firstOrder.preventDefault(firstOrder);
    $('.underlay').fadeIn();
    $('.--first-order').fadeIn();
  });

  $('.first__order__block__btn').click(function(firstOrderSecond) {
    firstOrderSecond.preventDefault(firstOrderSecond);
    $('.underlay').fadeIn();
    $('.--first-order-second-form').fadeIn();
  });

  /*Fixed menu on mobile*/
  if (winWidth > 319 && winWidth < 1024) {
    var menuPos = $('.menu__wrap').offset().top;
    menuPos = menuPos + 50;

    $(window).scroll(function() {
      if ($(this).scrollTop() > menuPos) {
        $('.menu__wrap').addClass('fixed');
      } else {
        $('.menu__wrap').removeClass('fixed');
      }
    });
  }
  /*Fixed menu on desktop*/
  if (winWidth > 1023) {
    var menuPos = $('.menu__wrap').offset().top;
    menuPos = menuPos + 150;

    $(window).scroll(function() {
      if ($(this).scrollTop() > menuPos) {
        $('.static__scrolled__menu__wrap').addClass('opened');
      } else {
        $('.static__scrolled__menu__wrap').removeClass('opened');
      }
    });
  }

  /*Scroll to sections*/
  $('.menu__item').click(function(j) {
    j.preventDefault(j);
    $('html, body').animate({scrollTop: $($.attr(this, 'href')).offset().top - 75}, 500);

    if ($(window).width() < 1024) {
      $('.hamburger').removeClass('is-active');
      $(this).parents('.menu').slideUp();
    }
  });

  /*Scroll to services*/
  $('.top__tabs__list__item__link').click(function() {
    $('html, body').animate({scrollTop: $($.attr(this, 'href')).offset().top - 75}, 500);
  });

  /*Phone mask*/
  $('.callback__popup__form input[type="tel"]').inputmask({
    "mask": "8 (999) 999-99-99",
    "clearIncomplete": true,
    "onincomplete": function(){$(this).next('.callback__popup__submit').attr("disabled", true)},
    "oncomplete": function(){$(this).next('.callback__popup__submit').attr("disabled", false)}
  });

  $('.consult__form__input input[type="tel"]').inputmask({
    "mask": "8 (999) 999-99-99",
    "clearIncomplete": true,
    "onincomplete": function(){$('.consult__form__button button').attr("disabled", true)},
    "oncomplete": function(){$('.consult__form__button button').attr("disabled", false)}
  });

  /*Product types images*/
  $('.products__types__switch').click(function(){

    var switchData = $(this).data('switch');

    $(this).parents('.products__types__switches__item').toggleClass('active');
    $(this).parents('.products__types__tabs__content__item').find('.products__types__images__item[data-switch="'+switchData+'"]').toggleClass('active');
  });

  $('.products__types__switch').hover(

    function() {

      var switchData = $(this).data('switch');
      $(this).parents('.products__types__tabs__content__item').find('.products__types__images__item[data-switch="'+switchData+'"]').addClass('hover');

    }, function() {

      var switchData = $(this).data('switch');
      $(this).parents('.products__types__tabs__content__item').find('.products__types__images__item[data-switch="'+switchData+'"]').removeClass('hover');
    }
  );

  // checkOffset();

  // $(document).scroll(function() {
  //   checkOffset();
  // });

  // function checkOffset() {
  //     if($('.top__animated__arrow').offset().top + $('.top__animated__arrow').height() >= $('.top__tabs__wrap').offset().top - 10)
  //         $('.top__animated__arrow').removeClass('fixed');
  //     if($(document).scrollTop() + window.innerHeight < $('.top__tabs__wrap').offset().top)
  //         $('.top__animated__arrow').addClass('fixed');
  //   }
});